const fs = require('fs');

const requesthandler = (req, res) => {
    const url = req.url;
    const method = req.method;

    if(url === "/"){
        res.write('<html>');
        res.write('<head><title>Enter Message</title></head>');
        res.write('<body>');
        res.write('<h1>SOme Greeting message</h1>');
        res.write('<form action="/create-user" method="POST">');
        res.write('<input type="text" name="username"><button>UserName</button>');
        res.write('</form>');
        res.write('</body>');
        res.write('</html>');
        return res.end();
    }
    if(url === "/users"){
        res.write('<html>');
        res.write('<head><title>Users</title></head>');
        res.write('<ul><li>user 1</li></ul>');
        res.write('<ul><li>user 2</li></ul>');
        res.write('<body>');

    }
    
    if(url === '/create-user' && method === 'POST'){
        const body = [];
        req.on('data', (chunk)=>{
            // console.log(chunk);
            body.push(chunk);
        });

        return req.on('end', ()=>{
            const parsedBody = Buffer.concat(body).toString();
            const message = parsedBody.split('=')[1];
            // console.log(message);
            // fs.writeFile('message.txt', message, err=>{
            //     res.status = 302;
            //     res.setHeader('Location', '/');
            //     return res.end();
            // }); 
            res.status = 302;
            res.setHeader('Location', '/');
            res.end();
            console.log(message);
        })
        
    }
};

module.exports = {
    handler: requesthandler
}