// function test(a, b){
//     return a + b;
// }

// test = (a,b) => {
//     return a + b;
// }

// a = test(1, 5);
// console.log(a);

const person = {
    name:  'meseek',
    age : 35,
    greet_style(){
        console.log("Hi it is me, mr " + this.name + "!!");
    }
}

const newPerson = {...person};
newPerson.name = "keeseek";
console.log(newPerson);