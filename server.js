
const http = require('http');

//creating a server and adding its functionality as an argument
const server =  http.createServer((req, res) => { 
    console.log(req); //shows the whole request
    console.log(req.url, req.method, req.headers); // shows selected part of the request
    // process.exit();
    // now starts the response part
    res.setHeader('Content-Type', 'text/html');
    res.write('<html>');
    res.write('<head><title>My first node response page</title></head>');
    res.write('<body><h1>Hello from Node js</h1></body>');
    res.write('</html>');
    res.end();
});

server.listen(3000);

