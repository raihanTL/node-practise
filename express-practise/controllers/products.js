const Product = require('../models/product');

exports.getAddProducts = (req, res, next) => {
    res.render('add-product',{
        pageTitle: 'Add Product',
        path: '/admin/add-products',
        formsCSS: true,
        productCSS: true,
        activeAddProduct: true
    });
};

exports.postAddProducts = (req, res, next) => {
    // console.log(req.body.title);
    // products.push({title: req.body.title});
    const product = new Product(req.body.title);
    product.save();
    res.redirect('/');
};

exports.getProducts = (req, res, next) => {
    // const products = adminData.products;
    const products = Product.fetchAll();
    res.render('shop',  {
        prods: products,
        pageTitle: 'Shop',
        path: '/',
        hasProducts: products.length > 0,
        activeShop: true,
        productCSS: true
    });
};



