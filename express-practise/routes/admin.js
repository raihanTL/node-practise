const express = require('express');
const path = require('path');

const router = express.Router();

// const rootDir = require('../util/path');
const productController = require('../controllers/products.js');

router.get('/add-products', productController.getAddProducts);

router.post('/products', productController.postAddProducts);

module.exports.routes = router;
// module.exports.products = products;