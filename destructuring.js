const person = {
    name:  'meseek',
    age : 35,
    greet_style(){
        console.log("Hi it is me, mr " + this.name + "!!");
    }
}

let games = ['cricket', 'football', 'hockey'];

const { name, age } = person;
// console.log(name, age);

const [a, b, c] = games;
console.log(a, b, c);