// function add(a, b){
//      return a + b;
// }

// const add = (a, b) => {
//     return a + b;
// }

// ans = add(1, 2);
// console.log(ans);

// const person = {
//     name:  'meseek',
//     age : 35,
//     greet_style(){
//         console.log("Hi it is me, mr " + this.name + "!!");
//     }
// }

// person.greet_style();

const games = ['cricket', 'football', 'hockey'];
for(let game of games){
    console.log(game);
}

alt_games = games.map(game => {
    return 'Game:' + game;
});
console.log(alt_games);
new_games = [...games];
console.log(new_games);

function test(...args){
    return args;
}

console.log(test(1,2,3,4));